FROM openjdk:12-alpine

COPY ./build/libs/cars-api.jar /app/cars-api.jar

ENTRYPOINT ["java", "-jar", "/app/cars-api.jar"]